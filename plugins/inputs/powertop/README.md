# Powertop Input Plugin

The `powertop` input plugin generate a `powertop.csv` USING the powrtop command to collects electrical consumption

## Host setup

- on `Linux`
- Install powertop with: `apt-get install powertop`

And that it

## Configuration

```toml
[[inputs.powertop]]
  ## Set the timer
  #  timer = 20.0

  ## Add more details
  # show_software_power = false

  ## file csv path (include the name of the file)
  # csv_path = "/usr/src/powertop.csv"
```

## Metrics

- software power consumer:
  - tags:
    - category
    - description
  - fields:
    - `usage/s`
    - `wakeups/s`
    - `(software) PW estimate [W]`
    - `total power [W]`

- device power report:
  - tags:
    - device name
  - fields:
    - `usage [%]`
    - `(device) PW estimate [W]`
