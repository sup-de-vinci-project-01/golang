package powertop

import (
	"github.com/influxdata/telegraf/testutil"
	"github.com/stretchr/testify/require"
	"reflect"
	"testing"
)

func TestCreatePowertopFile(t *testing.T) {
	got := CreatePowertopFile("powertop", 20.0)
	if got == nil {
		t.Logf(`CreateFile("powertop") success, expected %v, got %v,`, "<nil>", got)
	}
	if got != nil {
		t.Logf(`CreateFile("powertop") success, expected %v, got %v,`, "<error>", got)
	}
}

func TestFindAndConvertPrefix(t *testing.T) {
	values := map[string]float64{
		"2.2 mW":  0.0022,
		"2.2":     2.2,
		"10 W":    10,
		"10 um/s": 9.999999999999999e-06,
		"10 nm/s": 1e-8,
		"1.0 kW":  1000,
		"":        0,
	}

	for key, value := range values {
		got := FindAndConvertPrefix(key)
		if got == value {
			t.Logf(`FindAndConvertPrefix(%v) success, expected %v, got %v,`, key, value, got)
		} else {
			t.Errorf(`FindAndConvertPrefix(%v) failed, expected %v, got %v,`, key, value, got)
		}
	}
}

func TestPowertopOutputFilter(t *testing.T) {
	_, geterror := PowertopOutputFilter("sed", "-n", "/Usage;Device Name;PW Estimate/,/^_/p", "file_not_defined.csv")
	if geterror != nil {
		t.Logf(`PowertopOutputFilter("sed", "-n", "/Usage;Device Name;PW Estimate/,/^_/p", "file_not_defined.csv") success, expected "exit status 2", got %v,`, geterror)
	} else {
		t.Errorf(`PowertopOutputFilter("sed", "-n", "/Usage;Device Name;PW Estimate/,/^_/p", "file_not_defined.csv") failed, expected "<erro>", got %v,`, geterror)
	}

	_, geterror = PowertopOutputFilter("sed", "-n", "/Usage;Device Name;PW Estimate/,/^_/p", "./testdata/fake_data_error.csv")
	if geterror != nil {
		t.Logf(`PowertopOutputFilter("sed", "-n", "/Usage;Device Name;PW Estimate/,/^_/p", "./testdata/fake_data_error.csv") success, expected "Data not found", got %v,`, geterror)
	} else {
		t.Errorf(`PowertopOutputFilter("sed", "-n", "/Usage;Device Name;PW Estimate/,/^_/p", "./testdata/fake_data_error.csv") failed, expected "Data not found", got %v,`, geterror)
	}

	out, geterror := PowertopOutputFilter("sed", "-n", "/Usage;Device Name;PW Estimate/,/^_/p", "./testdata/powertop.csv")
	if geterror == nil {
		t.Logf(`PowertopOutputFilter("sed", "-n", "/Usage;Device Name;PW Estimate/,/^_/p", "./testdata/powertop.csv") success, expected "[][]string", got %v,`, reflect.TypeOf(out))
	} else {
		t.Errorf(`PowertopOutputFilter("sed", "-n", "/Usage;Device Name;PW Estimate/,/^_/p", "./testdata/powertop.csv") failed, expected "[][]string", got %v,`, geterror)
	}
}

func TestPowertop(t *testing.T) {
	p := &Powertop{
		Timer:             5.0,
		ShowSoftwarePower: true,
		CsvPath:           "/tmp/powertop.csv",
	}
	var acc testutil.Accumulator
	require.NoError(t, p.Gather(&acc))
	out, err := PowertopOutputFilter("sed", "-n", "/Usage;Wakeups\\/s;GPU ops\\/s;/,/^$/p", "/tmp/powertop.csv")
	if err != nil {
		t.Errorf("AddFieldsSoftwarePowerConsumer failed, execpected 'ok', but return %v", err)
	}
	var total float64
	for _, line := range out[1:] {
		// if my value is empty
		if line[7] == "" || line[7] == "0" {
			continue
		}
		//fmt.Println(line)
		tags := map[string]string{
			"category":     line[5],
			"description:": line[6],
		}
		fields := make(map[string]interface{})
		fields["usage/s"] = FindAndConvertPrefix(line[0])
		fields["wakeups/s"] = FindAndConvertPrefix(line[1])
		fields["(software) PW estimate [W]"] = FindAndConvertPrefix(line[7])
		total += FindAndConvertPrefix(line[7])
		acc.AssertContainsTaggedFields(t, "software power consumer", fields, tags)
	}
	//totalField := make(map[string]interface{})
	//totalField["total power [mW]"] = total
	//acc.AssertContainsTaggedFields(t, "software power consumer", totalField, map[string]string{})
}
