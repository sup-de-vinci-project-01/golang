FROM ubuntu:20.04

RUN apt-get update -y && \
    apt-get install -y powertop git sudo wget && \
    mkdir -p /usr/local/telegraf/bin
COPY build/linux-amd64 /usr/local/telegraf/bin

ENV PATH "$PATH:/usr/local/telegraf/bin"
# Installation influx && influxd
RUN wget -o pipefail get https://dl.influxdata.com/influxdb/releases/influxdb2-2.0.6-linux-amd64.tar.gz; \
     tar zxf influxdb2-2.0.6-linux-amd64.tar.gz
RUN ["/bin/bash", "-c","cp influxdb2-2.0.6-linux-amd64/{influx,influxd} /usr/local/bin/"]
RUN rm -r influxdb2-2.0.6-linux-amd64.tar.gz influxdb2-2.0.6-linux-amd64

# Install InfluxDB as a service with systemd
RUN wget -o pipefail get https://dl.influxdata.com/influxdb/releases/influxdb2-2.0.6-amd64.deb; \
    dpkg -i influxdb2-2.0.6-amd64.deb \
    && rm influxdb2-2.0.6-amd64.deb \
    && touch telegraf.conf \
    && mv telegraf.conf /usr/src \
    && chmod 0644 /usr/src/telegraf.conf

EXPOSE 8086
ENTRYPOINT service influxdb start && tail -F keep running
