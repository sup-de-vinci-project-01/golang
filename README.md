# Projet Sup de Vinci: Golang

[Lien du plugin powertop](https://gitlab.com/sup-de-vinci-project-01/golang/-/tree/master/plugins/inputs/powertop)

## Prérequis 

1. Avoir Installé `Docker` sur la machine hôte

2. Avoir Installé `docker-compose`

## Configuration docker-compose

```yml
version: "3.9"
services:
  go:
    container_name: powertop
    image: registry.gitlab.com/sup-de-vinci-project-01/golang
    ports:
      - "8086:8086"
    privileged: true
    volumes:
      - ./telegraf.conf:/usr/src/telegraf.conf
      - data:/var/lib/influxdb
      - config:/root/.influxdbv2/configs
volumes:
  config:
  data:
```
## Installation et configuration

1. Avoir le `docker-compose.yml` et un `telegraf.conf`. Vous pouvez copier le fichier [telegraf.conf](#fichier-telegraf) plus bas

2. Faire un `docker-compose up --detach` 

> Ne pas oublier le `--detach` car l'image lance un script en boucle pour éviter que celle-ci s'arrête

3. Parcourir l'interface influxdb sur le PORT `8086` (port par défaut), puis suivez les instuctions suivantes:

    1. Remplissez les champs, donnez un nom à votre Bucket: par exemple `Bucket: powertop` et `Organization: projet`

    2. Créez une configuration `Telegraf` sur l'onglet `Telegraf` et cliquez sur le bouton `Create Configuration` puis choisissez `System`

    3. Récupérez le token et remplacez la variable `"$INFLUX_TOKEN"` se trouvant dans `telegraf.conf` **OU** faites simplement un `export INFLUX_TOKEN=votre_token` sur votre machine et regardez l'étape `4.1`

> Après ça, vous devriez juste vous connecter en entrant vos identifiants après la fermeture de l'onglet.

> Vous pouvez télécharger le fichier `telegraf.conf` à partir de l'interface influxdb dans la partie Telegraf ou juste copier le fichier [telegraf.conf](#fichier-telegraf) plus bas et l'adapter à vos besoins.


4. Exécutez le container: `docker exec powertop telegraf -config /usr/src/telegraf.conf`

> Si vous le désirez, vous pouvez le laisser exécuter en fond avec l'option `-d`

Vous pouvez dès à présent visualiser les data dans la section `Explorer` et parcourir votre `Bucket`.

---

4.1 Dans le cas où avez fait un `export INFLUX_TOKEN=votre_token` executer la commande suivante `docker exec --env INFLUX_TOKEN powertop telegraf -config /usr/src/telegraf.conf`
## 🚩 A savoir

Pour que la collection marche correctement, pensez TOUJOURS à avoir une `interval` (dans [agent] ) **SUPERIEUR** au `timer` de powertop (dans [[inputs.powertop]] ).

## Des problèmes ?

Avez-vous mis le bon TOKEN ? Avez-vous bien fait un `export INFLUX_TOKEN=votre_token` ?

Pour visualiser le problème: `docker exec powertop telegraf -config /usr/src/telegraf.conf -debug`

Si cela ne vous aide toujours pas à résoudre le problème: faites un issue !

## Fichier telegraf

```toml
# Configuration for telegraf agent
[agent]
  ## Default data collection interval for all inputs
  interval = "30s"
  ## Rounds collection interval to 'interval'
  ## ie, if interval="10s" then always collect on :00, :10, :20, etc.
  round_interval = true

  ## Telegraf will send metrics to outputs in batches of at most@
  ## metric_batch_size metrics.
  ## This controls the size of writes that Telegraf sends to output plugins.
  metric_batch_size = 1000

  ## For failed writes, telegraf will cache metric_buffer_limit metrics for each
  ## output, and will flush this buffer on a successful write. Oldest metrics
  ## are dropped first when this buffer fills.
  ## This buffer only fills when writes fail to output plugin(s).
  metric_buffer_limit = 10000

  ## Collection jitter is used to jitter the collection by a random amount.
  ## Each plugin will sleep for a random time within jitter before collecting.
  ## This can be used to avoid many plugins querying things like sysfs at the
  ## same time, which can have a measurable effect on the system.
  collection_jitter = "0s"

  ## Default flushing interval for all outputs. Maximum flush_interval will be
  ## flush_interval + flush_jitter
  flush_interval = "10s"
  ## Jitter the flush interval by a random amount. This is primarily to avoid
  ## large write spikes for users running a large number of telegraf instances.
  ## ie, a jitter of 5s and interval 10s means flushes will happen every 10-15s
  flush_jitter = "0s"

  ## By default or when set to "0s", precision will be set to the same
  ## timestamp order as the collection interval, with the maximum being 1s.
  ##   ie, when interval = "10s", precision will be "1s"
  ##       when interval = "250ms", precision will be "1ms"
  ## Precision will NOT be used for service inputs. It is up to each individual
  ## service input to set the timestamp at the appropriate precision.
  ## Valid time units are "ns", "us" (or "µs"), "ms", "s".
  precision = ""

  ## Logging configuration:
  ## Run telegraf with debug log messages.
  debug = false
  ## Run telegraf in quiet mode (error log messages only).
  quiet = false
  ## Specify the log file name. The empty string means to log to stderr.
  logfile = ""

  ## Override default hostname, if empty use os.Hostname()
  hostname = ""
  ## If set to true, do no set the "host" tag in the telegraf agent.
  omit_hostname = false
[[outputs.influxdb_v2]]	
  ## The URLs of the InfluxDB cluster nodes.
  ##
  ## Multiple URLs can be specified for a single cluster, only ONE of the
  ## urls will be written to each interval.
  ## urls exp: http://127.0.0.1:8086
  urls = ["http://localhost:8086"]

  ## Token for authentication.
  token = "$INFLUX_TOKEN"

  ## Organization is the name of the organization you wish to write to; must exist.
  organization = "projet"

  ## Destination bucket to write into.
  bucket = "powertop"
[[inputs.powertop]]
  ## Set the timer
  #  timer = 20.0

  ## Add more details
  # show_software_power = false

  ## file csv path (include the name of the file)
  # csv_path = "/usr/src/powertop.csv"
```
